# Maintainer:  Iyán Méndez Veiga <me (at) iyanmv (dot) com>
# Contributor: Shaun Ren <shaun DOT ren (at) linux (dOt) com>
# Contributor: Simon Hanna <simon DOT hanna (at) serve-me (dOt) info>

pkgbase=rtl88xxau
kernel=5.10
_extramodules=extramodules-${kernel}-rt-MANJARO
_linuxprefix=linux${kernel/./}-rt
pkgname=${_linuxprefix}-${pkgbase}-aircrack
pkgver=r1174_5.10.90_rt60_1
pkgrel=1
pkgdesc="Aircrack-ng kernel module for Realtek 88XXau (USB adapters only) network cards (8811au, 8812au and 8821au chipsets) with monitor mode and injection support"
url="https://github.com/aircrack-ng/rtl8812au"
license=('GPL')
arch=('x86_64')
makedepends=("${_linuxprefix}" "${_linuxprefix}-headers" 'git')
provides=("${pkgbase}=${pkgver%%_*}" "${_linuxprefix}-${pkgbase}")
replaces=('rtl8812au-aircrack-dkms-git')
conflicts=('rtl8812au-aircrack-dkms-git' 'rtl8812au-dkms-git' 'rtl8821au-dkms-git' 'rtl8814au-dkms-git' 'rtl8812au-inject-dkms-git')
groups=("$_linuxprefix-extramodules")
install="${pkgbase}.install"
source=('rtl88xxau::git+https://github.com/aircrack-ng/rtl8812au.git#branch=v5.6.4.2')
sha256sums=('SKIP')

pkgver() {
    cd ${srcdir}/${pkgbase}
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    _pkgver=$(git describe --long 2>/dev/null | sed 's/\([^-]*-g\)/r\1/;s/-/./g')
    printf "r%s" "$(git rev-list --count HEAD)_${_ver/-/_}"
}

prepare() {
    cd "${srcdir}/${pkgbase}"
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    sed -i "s/KVER  :=.*/KVER  := ${_kernver}/" "${srcdir}/${pkgbase}/Makefile"
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

    cd "${srcdir}/${pkgbase}"
    make
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    cd "${srcdir}/${pkgbase}"

    install -Dm755 "88XXau.ko" "${pkgdir}/usr/lib/modules/${_extramodules}/88XXau.ko"
    find "${pkgdir}" -name '*.ko' -exec xz {} +

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${pkgbase}.install"
}
